import cv2
import py_compile

def carDetection(grayStream,colorStream):
  car_cascade = cv2.CascadeClassifier('cars_cascade.xml')
  cars = car_cascade.detectMultiScale(grayStream,26,26)
  return cars
    
    

def trafficSignDetection(grayStream,colorStream):
    traffic_sign_cascade = cv2.CascadeClassifier('keepRight.xml')
    trafficSigns = traffic_sign_cascade.detectMultiScale(grayStream,26,26)
    for(x,y,w,h) in trafficSigns:
        cv2.rectangle(colorStream,(x,y),(x+w,y+h),(0,0,255),3)
    return colorStream

def greentrafficLightDetection(grayStream,colorStream):
    green_traffic_light_cascade = cv2.CascadeClassifier('greenLight.xml')
    greentrafficLights = green_traffic_light_cascade.detectMultiScale(grayStream,15,15)
    return greentrafficLights

def redtrafficLightDetection(grayStream,colorStream):
    red_traffic_light_cascade = cv2.CascadeClassifier('redLight.xml')
    redtrafficLights = red_traffic_light_cascade.detectMultiScale(grayStream,26,26)
    return colorStream

py_compile.compile('classifierCreation.py')
print("pyc file created successfully!")