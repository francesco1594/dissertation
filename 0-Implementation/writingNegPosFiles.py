import os.path

driveLetter = 'G:'

def writingNegativeDescFile():
  negativeTextFile = open(driveLetter +"\MCAST\\Thesis\\Prototype\\dataset\\trafficLightClassifierStuff\\greenLight\\bg.txt", "a")
  imgNum = 8527
  while imgNum <= 9059:
    try:
      if(os.path.exists(driveLetter +"\MCAST\\Thesis\\Prototype\\dataset\\trafficLightClassifierStuff\\greenLight\\neg" + "\img" + str(imgNum) + ".png") == True or
         os.path.exists(driveLetter +"\MCAST\\Thesis\\Prototype\\dataset\\trafficLightClassifierStuff\\greenLight\\neg" + "\img" + str(imgNum) + ".PNG") == True):
        print ("C:\\dataset\\neg\\img" + str(imgNum) + ".png")
        negativeTextFile.write("C:\\dataset\\neg\\img" + str(imgNum) + ".png" + "\n")
    except:
      imgNum +=1
    imgNum +=1
  negativeTextFile.close()

def writingPositiveDescFile():
  positiveDatFile = open(driveLetter +"\MCAST\\Thesis\\Prototype\\dataset\\trafficLightClassifierStuff\\greenLight\\info.dat", "a")
  imgNum = 0
  while imgNum <= 672:
    try:
      if(os.path.isfile(driveLetter +"\MCAST\\Thesis\\Prototype\\dataset\\trafficLightClassifierStuff\\greenLight\\pos" + "\img" + str(imgNum) + ".png") == True or
         os.path.isfile(driveLetter +"\MCAST\\Thesis\\Prototype\\dataset\\trafficLightClassifierStuff\\greenLight\\pos" + "\img" + str(imgNum) + ".PNG") == True):
            print("pos\img" + str(imgNum) + ".png" + " " + " 1 0 0 70 70" + "\n")
            positiveDatFile.write("pos\img" + str(imgNum) + ".png" + " 1 0 0 70 70" + "\n")   
            imgNum +=1
    except:
        imgNum +=1
  positiveDatFile.close()

writingNegativeDescFile()
#writingPositiveDescFile()
