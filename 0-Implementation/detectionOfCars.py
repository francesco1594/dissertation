import cv2
import numpy as np
import time

carDataSet1Cap = cv2.VideoCapture('jan28.avi')
carDataSet2Cap = cv2.VideoCapture('april21.avi')
carDataSet3Cap = cv2.VideoCapture('march9.avi')

car = cv2.CascadeClassifier('cars_cascade.xml')

while True:
  if carDataSet1Cap.isOpened() == False:
    carDataSet1Cap.open()

  if carDataSet2Cap.isOpened() == False:
    carDataSet2Cap.open()

  if carDataSet3Cap.isOpened() == False:
    carDataSet3Cap.open()
    
  ret,dataSet1Frame = carDataSet1Cap.read()
  ret2,dataSet2Frame = carDataSet2Cap.read()
  ret3,dataSet3Frame = carDataSet3Cap.read();

  dataSet1GrFrame = cv2.cvtColor(dataSet1Frame, cv2.COLOR_BGR2GRAY)
  cars = car.detectMultiScale(dataSet1GrFrame,40,40)
  #dataSet2GrFrame = cv2.cvtColor(dataSet2Frame, cv2.COLOR_BGR2GRAY)
  #dataSet3GrFrame = cv2.cvtColor(dataSet3Frame, cv2.COLOR_BGR2GRAY)

  for (x,y,w,h) in cars:
    cv2.rectangle(dataSet1Frame,(x,y),(x+w,y+h),(255,0,0),2)

    

  #dataSet1Edges = cv2.Canny(dataSet1GrFrame,100,100)
  
  cv2.imshow('Dataset 1 ',dataSet1Frame)
  #cv2.imshow('Dataset 2',dataSet2GrFrame)
  #cv2.imshow('Dataset 3',dataSet3GrFrame)
  #cv2.imshow('Dataset 1 edges', dataSet1Edges)

  if cv2.waitKey(1)& 0xFF == ord('q'):
    break

carDataSet1Cap.release()
carDataSet2Cap.release()
carDataSet3Cap.release()
cv2.destroyAllWindows()
