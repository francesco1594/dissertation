import urllib.request
import cv2
import numpy as np
import os
import time
from shutil import copyfile

driveLetter = 'G:'
carImagesPath = driveLetter + "/MCAST/Thesis/Prototype/dataset/carClassifierStuff/pos/"
trafficSignsNegativePath = driveLetter + "/MCAST/Thesis/Prototype/dataset/trafficSignClassiferStuff/NoEntry/neg/"
trafficSignsPosPath = driveLetter + "/MCAST/Thesis/Prototype/dataset/trafficSignClassiferStuff/pedestrianCrossing/"
trafficLightPosPath = driveLetter + "/MCAST/Thesis/Prototype/dataset/trafficLightClassifierStuff/greenLight/pos/"
trafficLightNegPath = driveLetter + "/MCAST/Thesis/Prototype/dataset/trafficLightClassifierStuff/greenLight/neg/"
negImagesPath = driveLetter + "/MCAST/Thesis/Prototype/dataset/carClassifierStuff/neg/"
datasetImgPath = driveLetter + "/MCAST/Thesis/Prototype/dataset/"
  
def get_store_pos_img_from_internet():
  pos_imgs_link = 'http://www.image-net.org/api/text/imagenet.synset.geturls?wnid=n02960352'
  pos_imgs_link2 = 'http://www.image-net.org/api/text/imagenet.synset.geturls?wnid=n04285008'
  pos_imgs_link3 = 'http://www.image-net.org/api/text/imagenet.synset.geturls?wnid=n03079136'
  pos_imgs_link4 = 'http://www.image-net.org/api/text/imagenet.synset.geturls?wnid=n04212165'
  neg_imgs_link1 = 'http://www.image-net.org/api/text/imagenet.synset.geturls?wnid=n02930645'
  neg_imgs_link2 = 'http://www.image-net.org/api/text/imagenet.synset.geturls?wnid=n03415486'
  neg_imgs_link3 = 'http://www.image-net.org/api/text/imagenet.synset.geturls?wnid=n03379828'

  pos_img_urls = urllib.request.urlopen(neg_imgs_link3).read().decode()

  if not os.path.exists(driveLetter + ':\MCAST\Thesis\Prototype\dataset'):
    os.makedirs('dataset')

  picNum = 5173;

  for i in pos_img_urls.split('\n'):
    try:
      print(i)

      urllib.request.urlretrieve(i, "dataset/"+ 'img' + str(picNum) +'.jpg')

      img = cv2.imread("dataset/"+ 'img' + str(picNum) +'.jpg', cv2.IMREAD_GRAYSCALE)
      resized_img = cv2.resize(img,(100,100))
      cv2.imwrite("dataset/"+ 'img' + str(picNum) +'.jpg',resized_img)
      picNum += 1
    except Exception as e:
      print(str(e))

def store_neg_raw_imgs():
  pos_imgs_link = ''
  pos_img_urls = urllib.request.urlopen(pos_imgs_link).read().decode()

  if not os.path.exists(driveLetter + ':\MCAST\Thesis\Prototype\dataset'):
    os.makedirs('dataset')

  picNum = 1;

  for i in pos_img_urls.split('\n'):
    try:
      print(i)
      urllib.request.urlretrieve(i, "dataset/"+ 'img' + str(picNum) +'.jpg')
      img = cv2.imread("dataset/"+ 'img' + str(picNum) +'.jpg', cv2.IMREAD_GRAYSCALE)
      resized_img = cv2.resize(img,(100,100))
      cv2.imwrite("dataset/"+ 'img' + str(picNum) +'.jpg',resized_img)
      picNum += 1
    except Exception as e:
      print(str(e))

def get_store_pos_img_from_computer():
  picNum = 546
  imgNum = 546
  num = "000"
  while imgNum <= 672:
    if imgNum == 10:
      num = "00"
    elif imgNum == 100:
      num = "0"
    elif imgNum == 1000:
      num = ""

    if(os.path.exists(trafficLightPosPath + "img" + str(imgNum) + ".png") == True):
        img = cv2.imread(trafficLightPosPath + "img" + str(imgNum) +'.png', cv2.IMREAD_GRAYSCALE)
        resized_img = cv2.resize(img,(70,70))
        cv2.imwrite(datasetImgPath + "img" + str(picNum) +'.png',resized_img)
        print (trafficLightPosPath  + "img" + str(imgNum) +'.png')
    if(os.path.exists(trafficLightPosPath + "img" + str(imgNum) + ".PNG") == True):
        img = cv2.imread(trafficLightPosPath + "img" + str(imgNum) +'.PNG', cv2.IMREAD_GRAYSCALE)
        resized_img = cv2.resize(img,(70,70))
        cv2.imwrite(datasetImgPath + "img" + str(picNum) +'.png',resized_img)
        print (trafficLightPosPath  + "img" + str(imgNum) +'.png')
   
    picNum += 1
    imgNum += 1
  else:
    print("File does not exists! , continuing!!")
    picNum += 1
    imgNum += 1
  

def get_store_neg_img_from_computer():
  picNum = 8527
  imgNum = 8527
  num = "000"
  while imgNum <= 9059:
    if imgNum == 10:
      num = "00"
    elif imgNum == 100:
      num = "0"
    elif imgNum == 1000:
      num = ""
    
    if(os.path.exists(trafficLightNegPath + "img" + str(imgNum) + ".png") == True):
        img = cv2.imread(trafficLightNegPath + "img" + str(imgNum) +'.png', cv2.IMREAD_GRAYSCALE)
        resized_img = cv2.resize(img,(70,70))
        cv2.imwrite(datasetImgPath + "img" + str(picNum) +'.png',resized_img)
        print (trafficLightNegPath  + "img" + str(imgNum) +'.png')
        picNum += 1
        imgNum += 1
    if(os.path.exists(trafficLightNegPath + "img" + str(imgNum) + ".PNG") == True):
        img = cv2.imread(trafficLightNegPath + "img" + str(imgNum) +'.PNG', cv2.IMREAD_GRAYSCALE)
        resized_img = cv2.resize(img,(70,70))
        cv2.imwrite(datasetImgPath + "img" + str(picNum) +'.png',resized_img)
        print (trafficLightNegPath  + "img" + str(imgNum) +'.png')
        picNum += 1
        imgNum += 1
    else:
        picNum += 1
        imgNum += 1
    
def removeUselessImages():
  imgNum = 10296
  while imgNum < 30639:
    try:
      print ("Deleting: " + carImagesPath  + "img" + str(imgNum) +'.png')
      os.remove(carImagesPath + "img" + str(imgNum) +'.png')
      imgNum += 1
    except:
      imgNum += 1
      continue;

def renameImgs():
    oldImgNum = 8527
    newImgNum = 9059
    while oldImgNum <= 9138:
        try:

            
            if(os.path.exists(trafficLightNegPath + "img" + str(oldImgNum) + ".png") == True):
                os.rename(trafficLightNegPath + 'img' + str(oldImgNum) + '.png', trafficLightNegPath + 'img' + str(newImgNum) + '.png')
                print("Rename img " + str(oldImgNum) + " to img " + str(newImgNum))
                newImgNum += 1
            if(os.path.exists(trafficLightNegPath + "img" + str(oldImgNum) + ".PNG") == True):
                os.rename(trafficLightNegPath + 'img' + str(oldImgNum) + '.PNG', trafficLightNegPath + 'img' + str(newImgNum) + '.PNG')
                print("Rename img " + str(oldImgNum) + " to img " + str(newImgNum))
                newImgNum += 1
            oldImgNum += 1
        except Exception as e:
            print(e)
            break

def renameImgs2(): #This method was needed to rename positive imgs
    imgNum = 20
    newImgNum = 8069
    moreImgsPath = trafficSignsNegativePath + "moreImgs" + "/img"
    while imgNum < 7615:
        try:
            if(os.path.exists(moreImgsPath + str(imgNum) + " (2).PNG") == True):
                os.rename(moreImgsPath + str(imgNum) + " (2).PNG",moreImgsPath + str(newImgNum) + ".png")
                print("Rename img " + str(imgNum) + " to img " + str(newImgNum))
                newImgNum += 1
            imgNum +=1
        except Exception as e:
            print(e)
            break

def checkImgNum():
    startImgNum = 8527
    endImgNum = 9059
    try:
      checkImgNumFile = open("C:\\Users\\User\\Desktop\\checkImgNum.txt", "w")
      while startImgNum <= endImgNum: 
        if(os.path.exists(trafficLightNegPath + "img" + str(startImgNum) + ".png") == True):
                print("img num:" + str(startImgNum) + " exists!")
                checkImgNumFile.write("img num:" + str(startImgNum) + " exists! \n")
                startImgNum +=1

        if(os.path.exists(trafficLightNegPath + "img" + str(startImgNum) + ".PNG") == True):
                print("img num:" + str(startImgNum) + " exists!")
                checkImgNumFile.write("img num:" + str(startImgNum) + " exists! \n")
                startImgNum +=1
        else:
            print("img num:" + str(startImgNum) + " does not exists!")
            checkImgNumFile.write("img num:" + str(startImgNum) + " does not exists! \n")
            startImgNum +=1 
    except Exception as e:
      print(e)


def copyAndPasteImg(sourceImgPath,destinationImgPath):
    imgNum = 601
    endImgNum = 650
    while (imgNum <= endImgNum + 1):
        try:
            copyfile(sourceImgPath,destinationImgPath + "img" +  str(imgNum) + ".png")
            print("copied " + destinationImgPath+ str(imgNum))
            imgNum += 1
        except Exception as e:
            print(e)

def renameImgsFromLISA(sourceImgPath,destinationImgPath,beginImgNum,endImgNum):
    imgName = 'pedestrianCrossing_1333397756.avi_image' 
    newImgNum = 387
    while beginImgNum <= endImgNum:
        os.rename(sourceImgPath + imgName + str(beginImgNum) + '.png',destinationImgPath + "img" + str(newImgNum) + ".png")
        print(destinationImgPath + "img" + str(newImgNum) + ".png")
        newImgNum += 1
        beginImgNum += 1
        
get_store_neg_img_from_computer()
