from PIL import ImageGrab
import cv2
import time
from numpy import ones,vstack
from numpy.linalg import lstsq
from ScreenProcessing import *
import numpy as np
from directkeys import PressKey,ReleaseKey, W, A, S, D
from classifierCreation import * 

def straight():
    PressKey(W)
    ReleaseKey(A)
    ReleaseKey(D)

def left():
    PressKey(W)
    PressKey(A)
    ReleaseKey(D)

def right():
    ReleaseKey(A)
    PressKey(W)
    PressKey(D)

def slowDown():
    ReleaseKey(W)
    ReleaseKey(A)
    ReleaseKey(D)
    
def stopCar():
    PressKey(S)
    
for i in list(range(4))[::-1]:
    print(i+1)
    time.sleep(1)
     
last_time = time.time()
while True:
    screen =  np.array(ImageGrab.grab(bbox=(0,40,800,640)))
    print('Frame took {} seconds'.format(time.time()-last_time))
    last_time = time.time()
    #new_screen,original_image, m1,s m2 = ScreenProcessing.process_img(screen)
    # cv2.imshow('window', new_screen)
    # cv2.imshow('window2',cv2.cvtColor(original_image, cv2.COLOR_BGR2RGB))
    gray_frame = cv2.cvtColor(screen, cv2.COLOR_BGR2GRAY)
    #cars = carDetection(gray_frame,screen)
    greenLights = greentrafficLightDetection(gray_frame,screen)
    
    #for(x,y,w,h) in cars:
     #   cv2.rectangle(screen,(x,y), (x+w,y+h), (255,0,0), 3)
      #  cv2.putText(screen,str(x) + "," + str(y),(x,y),cv2.FONT_HERSHEY_COMPLEX_SMALL,2,(0,0,255),0)

        #Wrong way cars
        #if(x <= 362 and y <= 281  and x >163 and y >= 240 
        #   and x >=184 and y <= 515):
         #   PressKey(S)
         #   time.sleep(2)
         #   ReleaseKey(S)
         #   break
    for(x,y,w,h) in greenLights:
        cv2.rectangle(screen,(x,y),(x+w,y+h),(0,255,0),3)
    

    cv2.imshow('detectionWindow',cv2.cvtColor(screen,cv2.COLOR_BGR2RGB))
        
        #if(x >= 470 and x <=600):
            #PressKey(S)
            #time.sleep(3)
            #ReleaseKey(S)
            #PressKey(W)
            #break;
    

     #if ScreenProcessing.m1 < 0 and ScreenProcessing.m2 < 0:
     #   right()
    #elif ScreenProcessing.m1 > 0  and ScreenProcessing.m2 > 0:
     #   left()
    #else:
     #   straight()
    
    #cv2.imshow('window',cv2.cvtColor(screen, cv2.COLOR_BGR2RGB))
    if cv2.waitKey(25) & 0xFF == ord('q'):
        cv2.destroyAllWindows()
        break
